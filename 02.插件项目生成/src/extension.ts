import * as vscode from "vscode";

export function activate(context: vscode.ExtensionContext): void {
  const commands = [
    // vscode.commands.registerCommand("caseBreak.caseBreak", caseBreak.caseBreak),
  ];
  commands.forEach((command) => context.subscriptions.push(command));
}

// this method is called when your extension is deactivated
export function deactivate() {}
