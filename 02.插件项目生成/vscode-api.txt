import * as vscode from "vscode";

// 获取vscode编辑窗口对象
const textEditor = vscode.window.activeTextEditor;
if (!textEditor) {
  return undefined;
}
// 获取选中的文本
const selection = textEditor.selection;
// 选中文本是否为空
if (selection.isEmpty) {
  return undefined;
}
//选中文本是不是单行
if (selection.isSingleLine) {
  return undefined;
}
// 选中文本开始行号
var startLine: number;
startLine = selection.start.line;
// 选中文本结束行号
var endLine: number;
endLine = selection.end.line;

var start_num = selection.start.character;
var end_num = selection.end.character;
var select_str = textEditor.document
  .lineAt(startLine)
  .text.substring(start_num, end_num);
console.log("选中的文本:" + select_str);
// 获取选中区域文本，每一行组成的数组
let lines = [];
for (let i = startLine; i <= endLine; i++) {
  lines.push(textEditor.document.lineAt(i).text);
}

// 获取编辑器窗口中第n行文本
textEditor.document.lineAt(i).text;



// 编辑器窗口中文本替换
  // 获取vscode编辑窗口对象
  const textEditor = vscode.window.activeTextEditor;
  if (!textEditor) {
    return undefined;
  }
  
let startLine = 0;
let endLine = 0;
let lines: any[] = [];
textEditor.edit((editBuilder) => {
  // 定义替换范围
  const range = new vscode.Range(
    startLine, //开始行号
    0, //开始行，第几个字符开始
    endLine, //结束行号
    textEditor.document.lineAt(endLine).text.length //结束行，第几个字符结束
  );
  editBuilder.replace(range, lines.join("\n"));
});

// 获取setting.json配置文件内容
vscode.workspace.getConfiguration("sortLines").get("sortEntireFile");

// vscode 右下角显示一个弹框
vscode.window.showInformationMessage("你好");
vscode.window.showErrorMessage("我是错误信息！");
vscode.window
  .showInformationMessage("是否要打开小茗同学的博客？", "是", "否", "不再提示")
  .then((result) => {
    if (result === "是") {
      exec(`open 'https://haoji.me'`);
    } else if (result === "不再提示") {
      // 其它操作
    }
  });
// 状态栏
vscode.window.setStatusBarMessage("你好，前端艺术家！");

// vscode类型
// 文本编辑器窗口类型
// textEditor: vscode.TextEditor

// 获取鼠标所在文件全路径
// c:\Users\Administrator\Desktop\新建文件夹 (2)\shenjiaolong.vue-helper-2.4.5\1.js
const fileName = document.fileName;
console.log("fileName: ", fileName);
// 获取文件所在父级目录
// c:\Users\Administrator\Desktop\新建文件夹 (2)\shenjiaolong.vue-helper-2.4.5
const workDir = path.dirname(fileName);
console.log("workDir: ", workDir);
// 获取光标所在处的单词
const word = textEditor.document.getText(
  textEditor.document.getWordRangeAtPosition(position)
);
console.log("word: ", word);
