import * as vscode from 'vscode'; // 安装依赖就不报错了，yarn install
import { posix } from "path"; // 安装依赖就不报错了吧？

export const add = () => {
  // 获取vscode编辑窗口对象
  const textEditor = vscode.window.activeTextEditor;
  if (!textEditor) {
    return undefined;
  }

  // 获取选中的文本对象
  const selection = textEditor.selection;

  // 选中文本是否为空
  if (selection.isEmpty) {
    return undefined;
  }

  // 选中文本开始行号
  var startLine = selection.start.line;

  // 选中文本结束行号
  var endLine = selection.end.line;

  // 选中文本，一行的开始位置
  var start_num = selection.start.character;

  // 选中文本，一行的结束位置
  var end_num = selection.end.character;

  // 选中的文本
  var select_str = textEditor.document
    .lineAt(startLine)
    .text.substring(start_num, end_num);

  // 文件总行数
  var lineCount = textEditor.document.lineCount;

  // 要新增的代码
  let lines_add: string[] = [];

  lines_add.push(textEditor.document.lineAt(lineCount - 1).text); // 括号中的哪一行的代码
  lines_add.push('<script>'); // 新增的代码

  // 编辑器窗口中文本替换
  textEditor.edit((editBuilder: string) => {
    // 定义替换范围
    const range = new vscode.Range(
      startLine, //开始行号
      start_num, //开始行，第几个字符开始
      endLine, //结束行号
      end_num //结束行，第几个字符结束
    );
    editBuilder.replace(range, lines_add.join('\n'));
  });

  // vscode 右下角显示一个警告弹框
  vscode.window.showWarningMessage(`请自己新增`);
  // vscode 右下角显示一个消息弹框
  vscode.window.showInformationMessage(`${select_str}添加完成`);
  // vscode 右下角显示一个错误信息弹框
  vscode.window.showErrorMessage(`错误`);

  // 向文件写入的内容
  const writeStr = `<template>

</template>`;
  const writeData = Buffer.from(writeStr, 'utf8'); // 安装依赖就不报错了吧？
  const folderUri = vscode.workspace.workspaceFolders[0].uri;
  const fileUri = folderUri.with({ path: posix.join(folderUri.path, 'src/' + target_path) }); // src为项目根目录下的文件夹
  // 消息选中弹框
  vscode.window.showInformationMessage('文件已存在是否要覆盖生成？若选择【否】，将打开该文件。', '是', '否').then((result: string) => {
    if (result == '否') {
      // 当前窗口，打开某个文件
      vscode.window.showTextDocument(fileUri);
      return undefined;
    } else {
      // 新建文件，无论几级都可以自动创建
      vscode.workspace.fs.writeFile(fileUri, writeData);
      // 新窗口，打开某个文件
      vscode.window.showTextDocument(fileUri, { preview: false });
      vscode.window.showInformationMessage('文件覆盖生成完毕！');
    }
  })

  // 向右分割编辑器
  vscode.commands.executeCommand('workbench.action.splitEditor')

  // 跳转到文件的第几行
  vscode.commands.executeCommand(
    'editor.action.goToLocations',
    textEditor.document.uri, //anchor uri and position
    new vscode.Position(1, 0), // 行号，第几个字符位置
    [], // results (vscode.Location[])
    'goto', // mode
    '跳转成功' // <- message
  );

  // 是否有<script>标签
  var flag_isHave_script = false;
  var line_have_script = 0;
  for (let i = 0; i < lineCount; i++) {
    var line_text = textEditor.document.lineAt(i).text;
    flag_isHave_script = line_text.includes('<script>')
    if (flag_isHave_script) {
      break;
    }
  }

}