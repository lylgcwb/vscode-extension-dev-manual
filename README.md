# vscode-extension-dev-manual
## vscode插件开发视频教程的代码
### 版权声明：该套教程版权是作者【mycopycopy】（陈文博）所有，侵权必究。这套教程我没有在淘宝等任何地方售卖。

### 该git项目是vscode插件开发视频教程对应每节课的代码。为了查看方便，对应章节代码在对应文件夹中。
vscode插件开发视频教程地址：https://www.bilibili.com/video/BV18i4y1s7eY?from=search&seid=2394786055810514642

#### 介绍
配套哔哩哔哩vscode开发视频
提供源码

#### 多希望一个有钱的大哥能看透我的倔强
git clone git@gitee.com:lylgcwb/vscode-extension-dev-manual.git  
<img src="https://gitee.com/lylgcwb/vscode-extension-dev-manual/raw/master/pro-imgs/weixin-donate.png" width="600" alt="微信捐赠码"/>


#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
